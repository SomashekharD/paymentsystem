package com.allstate.entities;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {
    private Payment payment ;

    @BeforeEach
    void setUp() {
        payment  = new Payment(123, new Date(), "Credit card", 10000, 411);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getId() {
        assertEquals(123, payment.getId());
    }

    @Test
    void setId() {
        payment.setId(456);
        assertEquals(456, payment.getId());

    }

    @Test
    void testToString() {
        String output = payment.toString();
        assertEquals(output.contains("Credit card"), true);
    }
}