//dao IT test
package com.allstate.dao;

import com.allstate.entities.Payment;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PaymentDbTest {

    @Autowired
    private PaymentDb dao;

    @Test
    public void findRowcount_Success() {
        assertTrue(dao.rowcount() >= 1);
    }


    @Test
    public void saveAndfindById_Success() {
        Date date = new Date(System.currentTimeMillis());
        Payment payment = new Payment(122, date, "netbanking", 1000, 12345);

        dao.save(payment);

        assertEquals(payment.getId(), dao.findByID(122).getId());
    }

    @Test
    public void findType_Success() {

        List<Payment> result = dao.findByType("credit card");

        assertTrue(result.size() >= 1);
    }



}
