package com.allstate.services;

import com.allstate.dao.PaymentDb;
import com.allstate.entities.Payment;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
class PaymentServiceTest {

    @Autowired
    private PaymentService paymentService;

    @BeforeEach
    void setUp() {

    }

    @AfterEach
    void tearDown() {
    }


    @Test
    void testRowCount_withMock() {
        PaymentService pmtService = mock(PaymentService.class);
        when(pmtService.rowcount()).thenReturn(4);
        assertEquals(4, pmtService.rowcount());
    }

    @Test
    void testSaveAndfindById() {
        Payment payment  = new Payment(111, new Date(), "Credit card", 10000, 411);
        paymentService.save(payment);
        Payment paymentInfo = paymentService.findById(111);

        assertEquals(payment.getId(), paymentInfo.getId());
    }

    @Test
    void testFindByType() {

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        Payment payment  = new Payment(1111, new Date(), "Debit card", 10000, 411);
        paymentService.save(payment);
        List<Payment> paymentList = paymentService.findByType("Debit card");
        String expectedResult = formatter.format(new Date());
        String actualResult = formatter.format(paymentList.get(0).getPaymentDate());
        assertEquals(expectedResult, actualResult);

    }

}