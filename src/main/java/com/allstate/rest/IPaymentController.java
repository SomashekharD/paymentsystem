package com.allstate.rest;

import com.allstate.entities.Payment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface IPaymentController {
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    String getStatus();

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    int rowcount();

    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    ResponseEntity<Payment> findById(@PathVariable("id") int id);

    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    ResponseEntity<List> findByType(@PathVariable("type") String type);

    @RequestMapping(value = "save" ,method = RequestMethod.POST)
    int save(@RequestBody Payment payment);
}
