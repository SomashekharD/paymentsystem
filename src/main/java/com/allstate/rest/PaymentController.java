package com.allstate.rest;

import com.allstate.entities.Payment;
import com.allstate.services.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Collection;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("api/payment")
public class PaymentController implements IPaymentController {
    @Autowired
    private PaymentService paymentService;
    Logger logger = LoggerFactory.getLogger(PaymentController.class);

    @Override
    @RequestMapping(value = "/status", method = RequestMethod.GET)
    public String getStatus()
    {
        logger.info("Payment status method");
        return "Payment Rest Api";
    }

    @Override
    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public int rowcount() {
        logger.info("Payment rowcount method");
        return paymentService.rowcount();
    }

    @Override
    @RequestMapping(value = "/findbyid/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {

        logger.info("Payment findById method");

        Payment payment = paymentService.findById(id);

        if (payment == null){
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }


    @Override
    @RequestMapping(value = "/findbytype/{type}", method = RequestMethod.GET)
    public ResponseEntity<List> findByType(@PathVariable("type") String type) {

        logger.info("Payment findByType method");

        List<Payment> paymentList= paymentService.findByType(type);

        if (paymentList.size() == 0){
            return new ResponseEntity<List>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<List>(paymentList,HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value = "save" ,method = RequestMethod.POST)
    public int save(@RequestBody Payment payment) {
        logger.info("Payment save method");
        return paymentService.save(payment);
    }

}
