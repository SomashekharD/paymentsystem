package com.allstate.services;

import com.allstate.dao.PaymentDb;
import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class PaymentServiceImpl implements PaymentService {
    @Autowired
    private PaymentDb dao;


    @Override
    public int rowcount() {
        return dao.rowcount();
    }

    @Override
    public Payment findById(int id) {
        return id > 0 ? dao.findByID(id) : null;
    }

    @Override
    public List<Payment> findByType(String type) {
        return  type != null & !type.isEmpty() ?
                dao.findByType(type) : new ArrayList<Payment>();

    }

    @Override
    public int save(Payment payment) {
        return dao.save(payment);
    }

}
