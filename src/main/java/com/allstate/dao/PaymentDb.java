package com.allstate.dao;

import com.allstate.entities.Payment;

import java.util.List;

public interface PaymentDb {

    int rowcount();
    Payment findByID(int id);
    List<Payment> findByType(String type);
    int save(Payment payment);
}
