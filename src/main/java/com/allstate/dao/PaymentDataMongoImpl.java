package com.allstate.dao;

import com.allstate.entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDataMongoImpl implements  PaymentDb {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public int rowcount() {
        List<Payment> paymentList = mongoTemplate.findAll(Payment.class);
        return paymentList.size();
    }

    @Override
    public Payment findByID(int id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("id").is(id));
        Payment payment = mongoTemplate.findOne(query,Payment.class);
        return payment;
    }

    @Override
    public List<Payment> findByType(String type) {
        Query query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> paymentList = mongoTemplate.find(query,Payment.class);
        return paymentList;
    }

    @Override
    public int save(Payment payment) {
        mongoTemplate.insert(payment);
        return 0;
    }
}
